﻿using FiveOhFirstDataCore.Data.Account;
using FiveOhFirstDataCore.Data.Services;
using FiveOhFirstDataCore.Data.Structures.Roster;

using Microsoft.AspNetCore.Components;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiveOhFirstDataCore.Components.Roster.Placed;

public partial class PlatoonStaff
{
#pragma warning disable CS8618 // Inject is never null.
	[Inject]
	public IRosterService Roster { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

	[Parameter]
    public PlatoonData Platoon { get; set; }
    [Parameter]
    public Trooper? Adjutant { get; set; } = null;
    [CascadingParameter(Name = "Airborne")]
    public bool Airborne { get; set; }

	protected override async Task OnAfterRenderAsync(bool firstRender)
	{
		await base.OnAfterRenderAsync(firstRender);

        if (firstRender 
			&& Airborne 
			&& Adjutant is null)
        {
			Adjutant = await Roster.GetAcklayAdjutantAsync();
			StateHasChanged();
		}
	}
}
